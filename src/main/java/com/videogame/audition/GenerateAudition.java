package com.videogame.audition;

import java.util.List;

public class GenerateAudition {
	
	Audition audition;
	
	public int performerAudition(List<Audition> arrayList)
	{

		int k=0;

		for(Audition a: arrayList)
		{
			String s= a.typeofPerformer();
			
			if(s.equalsIgnoreCase("performer"))
			{
				k++;
			}
		}
		return k;
		
	}

	public int dancerAudition(List<Audition> arrayList)
	{
		int k=0;
		for(Audition a: arrayList)
		{
			String s= a.typeofPerformer();
			if(s.contains("dancer"))
			{
				k++;
			}
		}
		return k;
		
	}
	public int vocalistAudition(List<Vocalist> arrayList)
	{
		int k=0;String key="G"; int volume=7;
		for(Vocalist a: arrayList)
		{
			String s= a.typeofPerformer(key,volume);
			if(s.contains("I sing in the Key of"))			{
				k++;
			}
		}
		return k;
		
	}

}
