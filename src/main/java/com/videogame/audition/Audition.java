package com.videogame.audition;

public abstract class Audition {
	
	protected static int uniqueId;
	private String Type;
	
	abstract String typeofPerformer();
	
	
	public Audition(int uniqueId,String Type) {
		this.uniqueId = uniqueId;
		this.Type= Type;
	}

	

}
