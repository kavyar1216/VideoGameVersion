package com.videogame.audition;

public class Vocalist extends Audition{
	
	private String key;
	private int volume;
	
	public Vocalist(int uniqueId, String type,String key) {
		super(uniqueId, type);
		this.key=key;
	}
	public Vocalist(int uniqueId, String type,String key,int volume) {
		super(uniqueId, type);
		this.key=key;
		this.volume=volume;
	}
	
	public String typeofPerformer(String key) {
		return "I sing in the Key of " + key + uniqueId;
	}
	public String typeofPerformer(String key,int volume) {
		return "I sing in the Key of "+key+" at the volume " +volume + uniqueId;
	}
	@Override
	String typeofPerformer() {
		return null;
	}
	

}
