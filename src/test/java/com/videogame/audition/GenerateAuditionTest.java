package com.videogame.audition;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;

public class GenerateAuditionTest {
	List<Audition> arrayList,arrayList1;
	List<Vocalist> arrayList2;
	GenerateAudition generateAudition =new GenerateAudition();

	@Before
	public void setUp() throws Exception {
		arrayList= new ArrayList<Audition>();
		Performer performer = new Performer(325, null);
		Performer performer1 = new Performer(324,null);
		Performer performer2 = new Performer(323,null);
		Performer performer3 = new Performer(321,null);
		arrayList.add(performer);arrayList.add(performer1);
		arrayList.add(performer2);arrayList.add(performer3);
		arrayList1= new ArrayList<Audition>();
		Dancer dancer=new Dancer(326,"Tap",null);
		Dancer dancer1=new Dancer(329,"Tap",null);
		arrayList1.add(dancer);		arrayList1.add(dancer1);
		arrayList2= new ArrayList<Vocalist>();
		Vocalist vocalist = new Vocalist(327,null,"G",7);
		arrayList2.add(vocalist);
		
	}

	@Test
	public void performerTest() {
		int expectedValue=4;
		int actualValue=generateAudition.performerAudition(arrayList);
		assertEquals(expectedValue,actualValue);
	}
	@Test
	public void dancerTest() {
		int expectedValue=2;
		int actualValue=generateAudition.dancerAudition(arrayList1);
		assertEquals(expectedValue,actualValue);
	}
	@Test
	public void vocalistTest() {
		int expectedValue=1;
		int actualValue=generateAudition.vocalistAudition(arrayList2);
		assertEquals(expectedValue,actualValue);
	}
}
