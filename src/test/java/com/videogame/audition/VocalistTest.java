package com.videogame.audition;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

public class VocalistTest {
	Vocalist vocalist;
	Vocalist vocalist1;
	String key="G";
	int volume= 5;
	int uniqueId =326;
	String type=null;


	@Before
	public void setUp() throws Exception {
		vocalist= new Vocalist(uniqueId,type,key);
		vocalist1= new Vocalist(uniqueId,type,key,volume);
	
	}

	@Test
	public void testkey() {
		String expectedValue="I sing in the Key of G326";
		String actualValue= vocalist.typeofPerformer(key);
		assertEquals(expectedValue,actualValue);
		}
	@Test
	public void testKeyVolume() {
		String expectedValue="I sing in the Key of G at the volume 5326";
		String actualValue= vocalist1.typeofPerformer(key,volume);
		
		assertEquals(expectedValue,actualValue);
		}
	@Test
	public void testNotNull() {
		String expectedValue="abc";
		String actualValue= vocalist1.typeofPerformer();
		
		assertNotEquals(expectedValue,actualValue);
		}

}
