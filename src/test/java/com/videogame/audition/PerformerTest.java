package com.videogame.audition;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

public class PerformerTest {
	
	Audition audition;

	@Before
	public void setUp() throws Exception {
		int uniqueId =324;
		String type=null;
		audition= new Performer(uniqueId,type);
	}

	@Test
	public void testType() {
		String expectedValue="performer";
		String actualValue= audition.typeofPerformer();
		assertEquals(expectedValue,actualValue);
		}
	@Test
	public void testNotType() {
		String expectedValue="pp";
		String actualValue= audition.typeofPerformer();
		assertNotEquals(expectedValue,actualValue);
		}

}
