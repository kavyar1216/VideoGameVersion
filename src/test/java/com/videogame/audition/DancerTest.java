package com.videogame.audition;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

public class DancerTest {
	Audition audition;

	@Before
	public void setUp() throws Exception {
		int uniqueId =325;
		String type=null;
		String style="tap";
		audition= new Dancer(uniqueId,type, style);
	
	}

	@Test
	public void testType() {
		String expectedValue="tap325  dancer";
		String actualValue= audition.typeofPerformer();
		assertEquals(expectedValue,actualValue);
		}
	@Test
	public void testNotType() {
		String expectedValue="dancer";
		String actualValue= audition.typeofPerformer();
		
		assertNotEquals(expectedValue,actualValue);
		}


}
